require 'test_helper'

class EvenementTest < ActiveSupport::TestCase
  test "future only returns future (including today) events" do
    assert Evenement.all.count == 3

    futurs = Evenement.futurs
    assert futurs.count == 2

    for evenement in futurs
      assert evenement.date >= Date.today
    end
  end
end
