class CreateEvenements < ActiveRecord::Migration
  def change
    create_table :evenements do |t|
      t.string :name
      t.date :date
      t.string :email
      t.text :description

      t.timestamps null: false
    end
  end
end
