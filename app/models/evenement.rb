class Evenement < ActiveRecord::Base

  validates_presence_of :name, message: 'Champ obligatoire'
  validates_presence_of :date, message: 'Champ obligatoire'
  validates_presence_of :email, message: 'Champ obligatoire'
  validates_format_of :email,
                      with: /\A[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}\z/i,
                      message: "L'adresse email est invalide"

  validates_presence_of :description, message: 'Champ obligatoire'
  validates_length_of :description, :maximum => 300, message: 'Trop long, maximum 300 caractères'

  def self.futurs
    where("DATE(date) >= ?", Date.today)
  end

  def date_fr
    date.strftime('%d/%m/%Y') if !date.nil?
  end
end
