json.array!(@evenements) do |evenement|
  json.extract! evenement, :id, :name, :date, :email, :description
  json.url evenement_url(evenement, format: :json)
end
