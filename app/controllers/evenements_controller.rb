class EvenementsController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :set_evenement, only: [:show, :edit, :update, :destroy]

  # GET /evenements
  # GET /evenements.json
  def index
    @evenements = Evenement.futurs.order(sort_column + " " + sort_direction)
  end

  # GET /evenements/1
  # GET /evenements/1.json
  def show
  end

  # GET /evenements/new
  def new
    @evenement = Evenement.new
  end

  # GET /evenements/1/edit
  def edit
  end

  # POST /evenements
  # POST /evenements.json
  def create
    @evenement = Evenement.new(evenement_params)

    respond_to do |format|
      if @evenement.save
        format.html { redirect_to @evenement, notice: 'L\'événement à été créé' }
        format.json { render :show, status: :created, location: @evenement }
      else
        format.html { render :new }
        format.json { render json: @evenement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /evenements/1
  # PATCH/PUT /evenements/1.json
  def update
    respond_to do |format|
      if @evenement.update(evenement_params)
        format.html { redirect_to @evenement, notice: 'L\'événement à été mis à jour'  }
        format.json { render :show, status: :ok, location: @evenement }
      else
        format.html { render :edit }
        format.json { render json: @evenement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evenements/1
  # DELETE /evenements/1.json
  def destroy
    @evenement.destroy
    respond_to do |format|
      format.html { redirect_to evenements_url, alert: 'L\'événement à été supprimé' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evenement
      @evenement = Evenement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evenement_params
      params.require(:evenement).permit(:name, :date, :email, :description)
    end

    #Get parameter sort if it matches a columns name
    def sort_column
      Evenement.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
    end

    #Get parameter direction if it asc or desc
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end
end
